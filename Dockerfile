FROM alpine

RUN apk update && apk add imagemagick bash file ffmpeg

COPY . /app

ENTRYPOINT [ "bash", "/app/trucador.sh" ]