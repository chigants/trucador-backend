#!/usr/bin/env bash
#
# Image converter to Telegram sticker specification
#

##########################################################################
#                      Pre-flight conditions checks                      #
##########################################################################

# Check if 'convert' is available
which convert > /dev/null 2>&1 || exit 1


##########################################################################
#                            Global variables                            #
##########################################################################

INPUT_DIR=/mailbox/input
OUTPUT_DIR=/mailbox/output

MAX_DIM=512
MAX_FPS=30
MAX_DUR="3s"
MAX_SIZ=256000

IMG_FORMAT=".png"
VIDEO_FORMAT=".webm"
VIDEO_CODEC="vp9"

##########################################################################
#                                  Main                                  #
##########################################################################

IN_NAME="$(ls $INPUT_DIR )"
OUT_NAME="$(echo $IN_NAME | rev | cut -d'.' -f2- | rev)"

WIDTH="$(convert $INPUT_DIR/$IN_NAME -print '%w' 2>/dev/null)"
HEIGHT="$(convert $INPUT_DIR/$IN_NAME -print '%h' 2>/dev/null)"

if [ $HEIGHT -gt $WIDTH ]
then
    DIM_IMG_FLAG='x'
    DIM_VIDEO_FLAG="scale=-1:$MAX_DIM"
else
    DIM_VIDEO_FLAG="scale=$MAX_DIM:-1"
fi

mime_type="$(file --mime-type $INPUT_DIR/$IN_NAME | cut -d' ' -f2)"

if [ $? -eq 0 ]
then
    if echo "$mime_type" | grep "image" # If it's an image
    then
        convert $INPUT_DIR/$IN_NAME \
                -resize ${DIM_IMG_FLAG}${MAX_DIM} \
                $OUTPUT_DIR/${OUT_NAME}${IMG_FORMAT}

    elif echo "$mime_type" | grep "video" # If it's a video
    then
        ffmpeg -i $INPUT_DIR/$IN_NAME \
               -fpsmax $MAX_FPS \
               -t $MAX_DUR \
               -fs $MAX_SIZ \
               -vcodec $VIDEO_CODEC \
               -vf $DIM_VIDEO_FLAG \
               -an -sn \
               $OUTPUT_DIR/${OUT_NAME}${VIDEO_FORMAT}
    fi
fi